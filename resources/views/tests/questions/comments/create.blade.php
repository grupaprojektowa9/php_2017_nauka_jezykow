@extends('layouts.app')

@section('content')
    <h2> Dodaj komentarz do pytania nr: {{$question->id}}: </h2>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="post" action="{{ route('tests.questions.comments.store', ['test' => $test, 'question' => $question]) }}">
        {{ csrf_field() }}
        <legend>formularz:</legend>
        <div class="form-group">
            <label for="testName" class="col-lg-2 control-label">Tytuł:</label>
            <div class="col-lg-10">
                <input type="text" class="form-control" id="commentTitle"name="commentTitle" value="{{ old("commentTitle") }}">
            </div>
        </div>
        <div class="form-group">
            <label for="testName" class="col-lg-2 control-label">Treść:</label>
            <div class="col-lg-10">
                <textarea class="form-control" id="commentContent"name="commentText" value="{{ old("commentText") }}"> </textarea>
            </div>
        </div>
        <br>
        <input class="btn btn-succes center" type="submit" value="Dodaj">
    </form>
    </div>
    <ul class="pager">
        <li class="previous "><a href="{{ route('tests.questions.comments.index', ['test' => $test, 'question' => $question])}}">&larr; Powrót</a></li>
    </ul>
@endsection