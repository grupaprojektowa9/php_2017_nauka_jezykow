<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('login with existing user');

$name = 'Jas Fasola';
$email = 'jas@fasola.com';
$password = 'jasfasooola1988';

$I->haveInDatabase('users', [
    'email' => $email,
    'name' => $name,
    'password' => password_hash($password, PASSWORD_DEFAULT)
]);

$I->amOnPage('/');

$I->click('Login');

$I->seeCurrentUrlEquals('/login');

$I->fillField('email', $email);
$I->fillField('password', 'Wrong password');

$I->click('button[type=submit]');

$I->seeCurrentUrlEquals('/login');

$I->see('These credentials do not match our records.');

$I->fillField('email', $email);
$I->fillField('password', $password);

$I->click('button[type=submit]');

$I->seeCurrentUrlEquals('/home');

$I->see($name, 'a.dropdown-toggle');
$I->see('Jesteś zalogowany!', 'div.panel > div.panel-body');

