@extends('layouts.app')

@section('content')

<h2>Komentarze do "{{ $question->question }}"</h2>
<h2>{{ $comment->title }}</h2>

<p>{{ $comment->text }}</p>

@endsection