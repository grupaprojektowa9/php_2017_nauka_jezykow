<?php

namespace App\Http\Controllers\Tests;

use App\Test;
use App\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;




class QuestionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @param Test $test
     * @return mixed
     */
    public function index(Test $test)
    {
        //$questions = Question::all();
        return view('tests.questions.index')->withTest($test);
    }

    /**
     * Show the form for creating a new resource.
     * @param Test $test
     * @return \Illuminate\Http\Response
     */
    public function create(Test $test)
    {
        return view('tests.questions.create')->withTest($test);

    }

    /**
     * Store a newly created resource in storage.
     * @param Test $test
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request, \App\Test $test)

    {
        $this->validate($request, [
            'sentence' => 'required',
            'translation' => 'required'
        ]);

        $question = new Question();


        $question->question = $request->sentence;
        $question->correct_answer = $request->translation;

        $question->test_id = $test->id;

        $question->word = '';
        $question->author_id = auth()->user()->id;

        $question->save();

        return redirect()->route('tests.questions.show', [ 'test' => $test, 'question' => $question]);

    }

    /**
     * Display the specified resource.
     * @param Test $test
     * @param  Question $question
     * @return \Illuminate\Http\Response
     */

    public function show(\App\Test $test, Question $question)

    {
        return view('tests.questions.show')->withTest($test)->withQuestion($question);
    }

    /**
     * Show the form for editing the specified resource.
     * @param Test $test
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\Test $test, Question $question)
    {
        if(auth()->user()->id != $question->author_id) {
            return redirect('/question/error');
        }

        return view('tests.questions.edit')->withTest($test)->withQuestion($question);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function update(Request $request, \App\Test $test, $id)
    public function update(Request $request, \App\Test $test, Question $question)

    {
        $this->validate($request, [
            'sentence' => 'required',
            'translation' => 'required'
        ]);

        //$question = Question::find($question->id);
        //$new_question = $question;

        $question->question = $request->sentence;
        $question->correct_answer = $request->translation;

        $question->test_id = $test->test_id;

        $question->word = '';
        $question->author_id = auth()->user()->id;

        $question->save();

        return redirect()->route('tests.questions.show', [ 'test' => $test, 'question' => $question]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Test $test, Question $question)
    {
        if(auth()->user()->id != $question->author_id) {
            return redirect('/question/error');
        }

        $question->delete();

        return view('tests.edit')->withTest($test)->withQuestion($question);
    }

    public function result(\App\Test $test, Question $question){
        return redirect()->route('tests.questions.resulr', [ 'test' => $test, 'question' => $question]);
    }
}
