# Language platform  

### 0. Prerequisites - required Composer update

```bash

# First remove old composer binary
sudo rm /usr/bin/composer

# Download latest archive
wget https://getcomposer.org/download/1.5.5/composer.phar

# Move downloaded file to bin directory
sudo mv composer.phar /usr/bin/composer

# Make it executable
sudo chmod +x /usr/bin/composer
```

### 1. Setup project

```bash

# Go to project directory
cd ~/Phpstorm/php_2017_nauka_jezykow

# Install packages
composer install

# Create configuration file
# It contains e.g. database credentials
cp .env.example .env

# Initialize encrpyption keys
# Used e.g. to encrypt cookies
php artisan key:generate

# Migrate database table
php artisan migrate

```

### 3. Run server

```bash
php artisan serve
# Should start at localhost:8000
```

### 4. Generate database dump for Codeception tests and run tests

```bash
mysqldump -u test test -p > tests/_data/dump.sql
```

And after that:

```bash
vendor/bin/codecept run
```

Remember that server has to run while executing tests!
