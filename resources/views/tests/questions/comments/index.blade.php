@extends('layouts.app')

@section('content')

<h2>Komentarze dla "{{ $question->id }}"</h2>

<ul>


        @foreach($comments as $comment)
        @if ($question->id == $comment->question_id)
            <div id="question--{{ $question->id }}" class="panel panel-default">
                <div class="panel-heading">{{$comment->title}}</div>
                <div class="panel-body">

                    <div class="row">
                        <div class="form-group col-lg-9">
                            <label class="control-label" >{{$comment->text}}</label>
                        </div>

                        @if ( (auth()->user()->id == $question->author_id) || ( auth()->user()->id == $comment->author_id ) )
                        <div class="col-lg-3">
                                <form method="post" action="{{ route('tests.questions.comments.destroy', ['test'=>$test, 'question' => $question, 'comment' => $comment]) }}">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <input class="btn btn-default mt__md" type="submit" value="Usuń">
                                </form>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        @endif
        @endforeach
</ul>


<a href="{{ route('tests.questions.comments.create', ['test' => $test, 'question' => $question]) }}" class="btn btn-primary">Dodaj komentarz...</a>

<ul class="pager">
    <li class="previous "><a href="{{ route('tests.questions.index', ['test' => $test, 'question' => $question])}}">&larr; Powrót</a></li>

</ul>


@endsection