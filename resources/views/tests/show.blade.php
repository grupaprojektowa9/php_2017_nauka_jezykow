@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-8">
            <h2>{{ $test->title }}</h2>
            <p>
                Opis: {{ $test->description }}
            </p>
            <p>
                Język: {{ $test->lang }}
            </p>
        </div>
        <div class="col-sm-12 col-md-4 col-lg-4">
            <?php
            if (isset($_COOKIE["test_result_".$test->id."_".Auth::user()->name]))  {

                echo '<div class="well test__result"> Twój wynik to:   <span class="bold">  ';
                echo $_COOKIE["test_result_".$test->id."_".Auth::user()->name ]."</span></div>";
            }
            else {
                echo '<div class="well test__result"> Test nie został jeszcze rozwiązany, spróbuj! </div>';
            }
            ?>

        </div>
    </div>


    <div class="row center-block">
        @if (auth()->user()->id == $test->author_id)
            <div class="col-lg-4 col-sm-4 col-sm-12 mt__md">
            <a href="{{ route('tests.questions.create', $test) }}" class="btn btn-default">Dodaj pytanie</a>
            </div>
        @endif
        <div class="col-lg-4 col-sm-4 col-sm-12 mt__md">
            <a href="{{ route('tests.questions.index', $test) }}" class="btn btn-primary">Rozwiaż</a>
        </div>
            @if (auth()->user()->id == $test->author_id)
                <div class="col-lg-4 col-sm-4 col-sm-12">
                <form method="post" action="{{ route('tests.destroy', $test) }}">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input class="btn btn-default mt__md" type="submit" value="Usuń">
                </form>
            @endif
        </div>
    </div>




    <ul class="pager mt-5">
        <li class="previous "><a href="{{ route('tests.index') }}">&larr; Powrót</a></li>
    </ul>



@endsection