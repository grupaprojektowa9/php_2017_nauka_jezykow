@extends('layouts.app')

@section('content')
    <h2> Edycja Testu: </h2>
    <a href="{{ route('tests.questions.create', $test) }}" class="btn btn-default edit_test-buttons">Dodaj pytanie</a>

    <ul>
        <br><br>
        @foreach($test->questions as $question)
            @if ($question->test_id == $test->id)
                <div id="question--{{ $question->id }}" class="panel panel-default">

                    <div class="panel-heading">Przetłumacz na polski: </div>
                    <form method="post" action="{{ route('tests.questions.destroy', ['test' => $test, 'question' => $question]) }}">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input class="btn btn-danger edit_test-buttons" type="submit" value="Usuń">
                    <a href="{{ route('tests.questions.edit', ['test' => $test, 'question' => $question]) }}" class="btn btn-primary edit_test-buttons">Edytuj</a>

                    <div class="panel-body">
                    <h4>{{ $question->question }}</h4>
                    </div>

                </div>
            @endif
        @endforeach
    </ul>
    <ul class="pager">
        <li class="previous "><a href="/tests">&larr; Powrót</a></li>
    </ul>
@endsection