@extends('layouts.app')

@section('content')
    <h2> Dodaj Pytanie: </h2>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="post" action="{{ route('tests.questions.store', $test) }}">
        {{ csrf_field() }}
        <legend>formularz:</legend>
        <div class="form-group">
            <label for="testName" class="col-lg-2 control-label">Fraza:</label>
            <div class="col-lg-10">
                <input type="text" class="form-control" id="questionSentence"name="sentence" value="{{ old("sentence") }}">
            </div>
        </div>
        <div class="form-group">
            <label for="testDesc" class="col-lg-2 control-label">Tłumaczenie:</label>
            <div class="col-lg-10">
                <input type="text" class="form-control" id="questionTranslation" name="translation" value="{{ old("translation") }}">
            </div>
        </div>
        <br>
        <input class="btn btn-succes center" type="submit" value="Stwórz">
    </form>
    </div>
    <ul class="pager">
        <li class="previous "><a href="{{ route('tests.show', $test)}}">&larr; Powrót</a></li>
    </ul>
@endsection