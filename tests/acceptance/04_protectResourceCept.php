<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('test only for logged users');

$name = 'Jas Fasola';
$email = 'jas@fasola.com';
$password = 'jasfasooola1988';

$I->haveInDatabase('users', [
    'email' => $email,
    'name' => $name,
    'password' => password_hash($password, PASSWORD_DEFAULT)
]);


$I->amOnPage('/tests');

$I->dontSeeCurrentUrlEquals('/tests');
$I->seeCurrentUrlEquals('/login');

$I->fillField('email', $email);
$I->fillField('password', $password);

$I->click('button[type=submit]');

$I->seeCurrentUrlEquals('/tests');
$I->see('Testy:', 'h3');
