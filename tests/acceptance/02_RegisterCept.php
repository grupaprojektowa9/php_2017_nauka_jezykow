<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('register new user');

$I->amOnPage('/');

$I->click('Register');

$I->seeCurrentUrlEquals('/register');

$name = 'Jas Fasola';
$email = 'jas@fasola.com';
$password = 'jasfasooola1988';

$I->fillField('name', $name);
$I->fillField('email', $email);
$I->fillField('password', $password);
$I->fillField('password_confirmation', $password);

$I->dontSeeInDatabase('users', [
    'email' => $email
]);

$I->click('button[type=submit]');

$I->seeCurrentUrlEquals('/home');

$I->seeInDatabase('users', [
    'email' => $email
]);

$I->see($name, 'a.dropdown-toggle');
$I->see('Jesteś zalogowany!', 'div.panel > div.panel-body');
