<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('see welcome page');

$I->amOnPage('/');

$I->seeInTitle('Laravel');
$I->see('Language platform', 'div.title');

$I->click('Wyswietl testy');
$I->seeCurrentUrlEquals('/login');
