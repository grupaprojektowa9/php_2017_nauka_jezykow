<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use App\Test;


class TestsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = Test::all();

        return view('tests.index')->withTests($tests);    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required'
        ]);



        $test = new Test();
        $test->title = $request->title;
        $test->description = $request->description;
        $test->lang = $request->lang;
        $test->author_id = auth()->user()->id;
        $test->save();

        return redirect()->route('tests.show', $test);    }

    /**
     * Display the specified resource.
     *
     * @param  Test $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        return view('tests.show')->withTest($test);
    }

    /**
     * @param Test $test
     * @param Question $question
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit(Test $test, Question $question)
    {
        if(auth()->user()->id != $test->author_id) {
            return redirect('/tests/error');
        }

        return view('tests.edit')->withTest($test)->withQuestion($question);
    }

    /**
     * @param Request $request
     * @param Test $test
     */
    public function update(Request $request, Test $test)
    {
        //
    }

    /**
     * @param Test $test
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Test $test)
    {
        if(auth()->user()->id != $test->author_id) {
            return redirect('/tests/error');
        }

        $test->delete();

        return redirect()->route('tests.index');
    }

}
