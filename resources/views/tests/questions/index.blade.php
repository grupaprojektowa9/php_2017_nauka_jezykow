@extends('layouts.app')

@section('content')

    <h2 class="display-3">Test: {{ $test->title }}</h2>
    <ul class="pager">
        <li class="previous "><a href="{{ route('tests.show', $test)}}">&larr; Powrót</a></li>

    </ul>

    <h3 class="display-3"> Pytania: </h3>


        @foreach($test->questions as $question)
            <div id="question--{{ $question->id }}" class="panel panel-default">
                <div class="panel-heading">Przetłumacz słowo:</div>
                <div class="panel-body">
                    <h4>{{ $question->question }}</h4>
                    <div class="row">
                        <div class="form-group col-lg-9">
                            <label class="control-label" for="answer--{{ $question->id }}">Twoje tłumaczenie:</label>
                            <input class="form-control input-lg" type="text" id="answer--{{ $question->id }}" >
                        </div>
                        <div class="col-lg-3">
                            <button class="btn btn-default"
                                    onclick="solveQuestion({{$question->id}},'{{(string)$question->correct_answer}}')"> Sprawdz </button>
                            <a href="{{ route('tests.questions.comments.index', ['test'=>$test, 'question' => $question])}}" class="btn btn-default">Komentarze</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <a href="{{ route('tests.show', $test)}}">
            <button type="submit" id="sendButton" class="btn btn-default" value="0">
                Wyślij odpowiedzi
            </button>
        </a>

    <script>


        function solveQuestion(id, answer) {
            let elem = document.getElementById(`question--${id}`);
            let input = document.getElementById(`answer--${id}`);
            var sendButton = document.getElementById("sendButton");

            if(String(input.value) == String(answer)){
                if(elem.classList.contains("panel-default") || elem.classList.contains("panel-danger"))
                    sendButton.value++;
                elem.classList.add("panel-success");
                elem.classList.remove("panel-default","panel-danger");

            } else
            {
                if(elem.classList.contains("panel-success"))
                    sendButton.value--;
                elem.classList.add("panel-danger");
                elem.classList.remove("panel-default");
                elem.classList.remove("panel-success");
            }
            docCookies.setItem("test_result_{{$test->id}}_{{ Auth::user()->name }}", sendButton.value);
        }
    </script>



@endsection

