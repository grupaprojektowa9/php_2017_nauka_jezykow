@extends('layouts.app')

@section('content')
    <h2>Dodawanie testu</h2>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="post" action="{{ route('tests.store') }}">
            {{ csrf_field() }}
            <legend>formularz:</legend>
            <div class="form-group">
                <label for="testName" class="col-lg-2 control-label">Nazwa testu:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="testName"name="title" value="{{ old("title") }}">
                </div>
            </div>
            <div class="form-group">
                <label for="testDesc" class="col-lg-2 control-label">Opis testu:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="testDesc" name="description" value="{{ old("description") }}">
                </div>
            </div>
            <div class="form-group">
                <label for="testLang" class="col-lg-2 control-label">Język testu:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="testLang"name="lang" value="{{ old("lang") }}">
                </div>
            </div>
            <br>
            <input class="btn btn-succes center" type="submit" value="Stwórz">
        </form>
    </div>
    <ul class="pager">
        <li class="previous "><a href="/tests">&larr; Powrót</a></li>
    </ul>
@endsection