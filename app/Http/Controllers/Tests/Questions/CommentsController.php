<?php

namespace App\Http\Controllers\Tests\Questions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tests\Question\Comment;
use App\Tests;
use App\Tests\Question;

class CommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\App\Test $test, \App\Question $question)
    {
        $comments = Comment::all();
        return view('tests.questions.comments.index')->withTest($test)->withQuestion($question)->withComments($comments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(\App\Test $test, \App\Question $question)
    {

        return view('tests.questions.comments.create')->withTest($test)->withQuestion($question);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, \App\Test $test, \App\Question $question)
    {

        $this->validate($request, [
            'commentTitle' => 'required',
            'commentText' => 'required'
        ]);


        $comment = new Comment();
        $comment->title = $request->commentTitle;
        $comment->text= $request->commentText;
        $comment->author_id = auth()->user()->id;

        $comment->question_id = $question->id;

        $comment->save();

        return redirect()->route('tests.questions.comments.index', ['test' => $test, 'question' => $question]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(\App\Test $test, \App\Question $question, Comment  $comment)
    {
        return view('tests.questions.comments.show')->withTest($test)->withQuestion($question)->withComment($comment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Test $test, \App\Question $question, Comment  $comment)
    {
        if(auth()->user()->id != $comment->author_id) {
            return redirect('/comments/error');
        }

        $comment->delete();
        return redirect()->route('tests.questions.comments.index', ['test' => $test, 'question' => $question]);


    }
}
