<?php 
/*$I = new AcceptanceTester($scenario);
$I->wantTo('have post creation system');


$I->amOnPage('/posts');
$I->see('Posts:', 'h2');

$I->seeLink('Create new...', '/posts/create');
$I->click('Create new...');

$I->seeCurrentUrlEquals('/posts/create');

$I->see('New post:', 'h2');

$title = "Some title";
$text = "Some text";

$I->fillField('title', $title);
$I->fillField('text', $text);

$I->dontSeeInDatabase('posts', [
    'title' => $title,
    'text' => $text
]);

$I->click('Create');

$I->seeInDatabase('posts', [
    'title' => $title,
    'text' => $text
]);

$id = $I->grabFromDatabase('posts', 'id', [
    'title' => $title,
    'text' => $text
]);

$I->seeCurrentUrlEquals('/posts/' . $id);

$I->see($title, 'h2');
$I->see($text, 'p');


$I->amOnPage('/posts');

$I->see($title, 'ul > li > strong');
$I->see($text, 'ul > li');

$I->click('details');

$I->seeCurrentUrlEquals('/posts/' . $id);

$I->click('edit');

$I->seeInField('title', $title);
$I->seeInField('text', $text);

$newTitle = 'New title';
$newText = 'New text';

$I->fillField('title', $newTitle);
$I->fillField('text', $newText);

$I->click('Update');


$I->seeCurrentUrlEquals('/posts/' . $id);

$I->see($newTitle, 'h2');
$I->see($newText, 'p');

$I->dontSeeInDatabase('posts', [
    'title' => $title,
    'text' => $text
]);

$I->seeInDatabase('posts', [
    'title' => $newTitle,
    'text' => $newText
]);

$I->click('Delete');

$I->seeCurrentUrlEquals('/posts');

$I->dontSeeInDatabase('posts', [
    'title' => $newTitle,
    'text' => $newText
]);*/

