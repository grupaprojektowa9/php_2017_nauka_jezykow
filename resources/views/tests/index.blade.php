@extends('layouts.app')

@section('content')
        <h3 class="display-2">Testy:</h3>

        <a href="{{ route('tests.create') }}" class="btn btn-primary">Dodaj test</a>

        <table class="table table-striped table-hover ">
            <thead>
              <tr>
                <th>ID</th>
                <th>Nazwa testu</th>
                  <th>Język</th>
                <th> </th>
                  <th> </th>

              </tr>
            </thead>
            <tbody>
                @foreach($tests as $test)
                <tr>
                    <td>{{ $test->id }}</td>
                    <td>{{ $test->title }}</td>
                    <td>{{ $test->lang }}</td>
                    <td><a class="badge badge-info" href="{{ route('tests.show', $test) }}">Rozwiąż</a></td>

                    @if (auth()->user()->id == $test->author_id)
                        <td><a class="badge badge-info" href="{{ route('tests.edit', $test) }}">Edytuj</a></td>
                    @else
                        <td><a class="badge badge-info" href="{{ route('tests.edit', $test) }}"></a></td>
                    @endif

                </tr>
            @endforeach
            </tbody>
        </table>

        <ul class="pager">
            <li class="previous "><a href="/">&larr; Powrót</a></li>
        </ul>
@endsection
