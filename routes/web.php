<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {


    Route::get('/protected', function () {

        return view('protected');
    });



    /**
     * Tests Controller
     */
    Route::resource('/tests', 'TestsController');
    Route::resource('/tests.questions', 'Tests\QuestionsController');
    Route::resource('/tests.questions.comments', 'Tests\Questions\CommentsController');



});


