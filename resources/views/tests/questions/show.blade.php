 @extends('layouts.app')

@section('content')

    <h2>Fraza: {{ $question->question }}</h2>
    <h2>Tłumaczenie: {{ $question->correct_answer }}</h2>

    

    <div class="row mt-5 mb-5">
        <div class="col-lg-4 mx-auto">
            <a href="{{ route('tests.questions.edit', ['test' => $test, 'question' => $question]) }}" class="btn btn-primary">Edytuj pytanie</a>
            <a href="{{ route('tests.edit', ['test' => $test, 'question' => $question]) }}" class="btn btn-primary">Edytuj test</a>

        </div>

        <div class="col-lg-4 mx-auto">
            <form method="post" action="{{ route('tests.questions.destroy', ['test' => $test, 'question' => $question]) }}">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <input class="btn btn-danger" type="submit" value="Usuń">
            </form>
        </div>
    </div>

    <ul class="pager">
        <li class="previous "><a href="{{ route('tests.show', $test)}}">&larr; Powrót</a></li>
    </ul>






@endsection