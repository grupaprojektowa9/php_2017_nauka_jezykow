<?php
/*$I = new AcceptanceTester($scenario);
$I->wantTo('write and browse book comments');

$I->amAuthenticatedAs("Foo Bar", "foo@bar.com", "foo123");

$bookIsbn = "1234512345123";
$bookTitle = "SomeTitle";
$bookDescription = "SomeDescription";

$bookId = $I->haveInDatabase('books', [
    'isbn' => $bookIsbn,
    'title' => $bookTitle,
    'description' => $bookDescription
]);

$I->amOnPage("/books/$bookId");

$I->see($bookIsbn);
$I->see($bookTitle);
$I->see($bookDescription);

$I->click("Reviews");

$I->seeCurrentUrlEquals("/books/$bookId/comments");

$I->see('Reviews for "' . $bookTitle . '"', 'h2');

$I->canSeeNumRecords(0, 'comments');

$reviewTitle = 'Good book';
$reviewText = 'This one took me by surprise...';

$reviewId = $I->haveInDatabase('comments', [
    'book_id' => $bookId,
    'title' => $reviewTitle,
    'text' => $reviewText
]);

$I->amOnPage("/books/$bookId/comments/$reviewId");

$I->see('Review for "' . $bookTitle . '"', 'h2');
$I->see($reviewTitle, 'h2');
$I->see($reviewText, 'p');

$I->amOnPage("/books/$bookId/comments");

$I->see($reviewTitle, 'ul > li > a');
$I->click($reviewTitle);

$I->seeCurrentUrlEquals("/books/$bookId/comments/$reviewId");

$I->amOnPage("/books/$bookId/comments");

$I->click("New review");

$I->seeCurrentUrlEquals("/books/$bookId/comments/create");

$I->see('New review for "' . $bookTitle . '"', 'h2');

$newReviewTitle = 'New review title';
$newReviewText = 'New review text';

$I->fillField('title', $newReviewTitle);
$I->fillField('text', $newReviewText);
$I->click('Create');

$I->seeInDatabase('comments', [
    'book_id' => $bookId,
    'title' => $newReviewTitle,
    'text' => $newReviewText
]);

$newReviewId = $I->grabFromDatabase('comments', 'id', ['title' => $newReviewTitle]);

$I->seeCurrentUrlEquals("/books/$bookId/comments/$newReviewId");

$otherBookId = $I->haveInDatabase('books', [
    'isbn' => '5432154321543',
    'title' => 'Title',
    'description' => 'Description'
]);

$I->amOnPage("/books/$otherBookId/comments");

$I->see('Reviews for "Title"', 'h2');

$I->dontSee($reviewTitle, 'ul > li > a');
$I->dontSee($newReviewTitle, 'ul > li > a');
*/