<?php

namespace App\Tests\Question;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function questions()
    {
        return $this->belongsTo(Question::class);
    }
}
