<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

   /**
    * Define custom actions here
    */

   public function amAuthenticatedAs($name, $email, $password)
   {
       $I = $this;

       $I->haveInDatabase('users', [
           'name' => $name,
           'email' => $email,
           'password' => password_hash($password, PASSWORD_DEFAULT)
       ]);


       $I->amOnPage('/login');

       $I->fillField('email', $email);
       $I->fillField('password', $password);

       $I->click('button[type=submit]');

       $I->seeCurrentUrlEquals("/home");

       $I->see('You are logged in!', 'div.panel > div.panel-body');
       $I->see($name);
   }
}
