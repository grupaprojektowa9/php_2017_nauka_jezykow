<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('Have tests page with add/delete options');

$myName = 'Jas Fasola';
$myEmail = 'jas@fasola.com';
$myPassword = 'jasfasooola1988';


$I->haveInDatabase('users', [
    'email' => $myEmail,
    'name' => $myName,
    'password' => password_hash($myPassword, PASSWORD_DEFAULT)
]);


$I->amOnPage('/tests');
$I->dontSee('Testy:', 'h3');
$I->seeCurrentUrlEquals('/login');

$I->fillField('email', $myEmail);
$I->fillField('password', $myPassword);

$I->click('button[type=submit]');

$I->seeCurrentUrlEquals('/tests');

$I->see('Testy:', 'h3');


$I->seeLink('Dodaj test', '/tests/create');
$I->click('Dodaj test');

$I->seeCurrentUrlEquals('/tests/create');

$I->see('Dodawanie testu', 'h2');

/*
$I->seeCurrentUrlEquals('/students/create');

$I->see('The index field is required.', 'li');
$I->see('The name field is required.', 'li');
$I->see('The surname field is required.', 'li');
$studentIndex = "123123";
$studentName = "SomeName";
$studentSurname = "SomeSurname";*/

$testName = "Angielski poziom A1 - nazwa_testu";
$testDesc = "Krotki opis testu";
$testLang = "lang_123";


$I->dontSeeInDatabase('tests', [
    'title' => $testName,
    'description' => $testDesc,
    'lang' => $testLang
]);


$I->fillField('Nazwa testu:', $testName);
$I->fillField('Opis testu:', $testDesc);
$I->fillField('Język testu:', $testLang);

$I->click('Stwórz');
$I->see("$testName", 'h2');

/*
$I->seeCurrentUrlEquals('/tests/create');
$I->see('The index must be 6 digits.', 'li');
$I->dontSee('The name field is required.', 'li');
$I->dontSee('The surname field is required.', 'li');

$I->fillField('index', $studentIndex);

*/



$I->seeInDatabase('tests', [
    'title' => $testName,
    'description' => $testDesc,
    'lang' => $testLang
]);

$id = $I->grabFromDatabase('tests', 'id', [
    'title' => $testName
]);

$I->seeCurrentUrlEquals('/tests/' . $id);

$I->see("$testName", 'h2');

$I->amOnPage('/tests');

$I->see("$testName", 'td');
$I->see("$testLang", 'td');

$I->click('Rozwiąż');

$I->seeCurrentUrlEquals('/tests/' . $id);
$I->see("$testName", 'h3');


$I->click('Usuń');

$I->seeCurrentUrlEquals('/tests');

$I->dontSeeInDatabase('tests', [
    'title' => $testName,
    'description' => $testDesc
]);


