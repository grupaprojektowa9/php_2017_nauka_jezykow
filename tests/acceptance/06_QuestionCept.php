<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('Have question page');

$myName = 'Jas Fasola';
$myEmail = 'jas@fasola.com';
$myPassword = 'jasfasooola1988';


$I->haveInDatabase('users', [
    'email' => $myEmail,
    'name' => $myName,
    'password' => password_hash($myPassword, PASSWORD_DEFAULT)
]);


$I->amOnPage('/tests');
$I->dontSee('Testy:', 'h3');
$I->seeCurrentUrlEquals('/login');
$I->fillField('email', $myEmail);
$I->fillField('password', $myPassword);
$I->click('button[type=submit]');

$I->seeCurrentUrlEquals('/tests');
$I->see('Testy:', 'h3');

$I->seeLink('Dodaj test', '/tests/create');
$I->click('Dodaj test');
$I->see('Dodaj Pytanie:', 'h3');

$questionDesc = "kot";
$questionTranslation = "cat";


$I->fillField('Fraza:', $questionDesc);
$I->fillField('Tłumaczenie:', $questionTranslation);

$I->click('Stwórz');
$I->seeInDatabase('questions', [
    'question' => $questionDesc,
    'correct_answer' => $questionTranslation,
]);

$I->see("$questionDesc", 'h2');
$I->see("$questionTranslation", 'h2');

$I->click("← Powrót");
